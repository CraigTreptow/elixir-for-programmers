defmodule TextClient.Impl.Player do
  @typep game :: Hangman.game()
  @typep tally :: Hangman.tally()
  @typep state :: {game, tally}

  @spec start(game) :: :ok
  def start(game) do
    tally = Hangman.tally(game)
    interact({game, tally})
  end

  @spec interact(state) :: :ok
  def interact({_game, _tally = %{game_state: :won}}), do: IO.puts("Congragulations.  You won!")

  def interact({_game, tally = %{game_state: :lost}}),
    do: IO.puts("Sorry, you lost. The word was #{tally.letters |> Enum.join()}.")

  def interact({game, tally}) do
    IO.puts(feedback_for(tally))
    IO.puts(current_word(tally))

    tally = Hangman.make_move(game, get_guess())
    interact({game, tally})
  end

  def feedback_for(tally = %{game_state: :initializing}),
    do: "Welcome! I'm thinking of a #{tally.letters |> length} letter word.\n"

  def feedback_for(%{game_state: :good_guess}), do: "Good guess!\n"
  def feedback_for(%{game_state: :bad_guess}), do: "Sorry, that letter's not in the word.\n"
  def feedback_for(%{game_state: :already_used}), do: "You already used that letter.\n"

  def get_guess() do
    IO.gets("Next letter: ")
    |> String.trim()
    |> String.downcase()
  end

  def current_word(tally) do
    [
      "Word so far: " <> IO.ANSI.white_background(),
      tally.letters |> Enum.join(" "),
      IO.ANSI.reset(),
      "  (turns left: ",
      tally.turns_left |> to_string(),
      ", used: ",
      used_letters(tally),
      ")\n"
    ]
  end

  def used_letters(_tally = %{used: []}), do: "None"
  def used_letters(tally), do: tally.used |> Enum.join(",")
end
